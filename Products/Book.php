<?php
namespace Products;

use Products\MainProduct;
use Model\DB;

class Book extends MainProduct
{
    public $weight;

    function __construct($request) {
        parent::__construct(
            $request['sku'],
            $request['name'],
            $request['price'],
            $request['type']
        );

        $this->characteristic = 'Weight'; 

        $this->weight = $request['weight'];

        $this->value = htmlspecialchars(strip_tags($this->weight . " KG"));
    }

}
