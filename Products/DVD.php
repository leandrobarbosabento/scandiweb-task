<?php
namespace Products;

use Products\MainProduct;
use Model\DB;

class DVD extends MainProduct
{

    public $size;

    function __construct($request) {
        parent::__construct(
            $request['sku'],
            $request['name'],
            $request['price'],
            $request['type']
        );

        $this->characteristic= 'Size';

        $this->size = $request['size'];

        $this->value=htmlspecialchars(strip_tags($this->size . " MB"));
    }

}
