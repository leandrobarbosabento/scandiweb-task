<?php
namespace Products;

use Model\DB;
abstract class MainProduct
{
    public $sku;
    public $name;
    public $price;
    public $type;
    public $characteristic;
    public $value;
    public $db;

    function __construct($sku, $name, $price, $type) {
        $this->sku = htmlspecialchars(strip_tags($sku));
        $this->name = htmlspecialchars(strip_tags($name));
        $this->price = htmlspecialchars(strip_tags($price));
        $this->type = htmlspecialchars(strip_tags($type));
    }

    public function insert(){

        $db = new DB();
        $conn = $db->connect();
        
        $query = "INSERT INTO products
                    SET
                        sku=:sku,
                        name=:name,
                        price=:price,
                        type=:type,
                        characteristic=:characteristic,
                        value=:value";
      
        $stmt = $conn->prepare($query);
      
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":characteristic", $this->characteristic);
        $stmt->bindParam(":value", $this->value);
      
        if($stmt->execute()){
            return true;
        }
      
        return false;
    }
}
