<?php
namespace Products;

use Products\MainProduct;
use Model\DB;

class Furniture extends MainProduct
{
    public $width;
    public $height;
    public $length;
    public $dimensions;

    function __construct($request) {
        parent::__construct(
            $request['sku'],
            $request['name'],
            $request['price'],
            $request['type']
        );

        $this->characteristic= 'Dimensions';

        $this->width  = $request['width'];
        $this->height = $request['height'];
        $this->length = $request['length'];
        
        $this->value=htmlspecialchars(strip_tags($this->height.'x'.$this->width.'x'.$this->length));
    }

}
