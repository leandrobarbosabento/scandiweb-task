<?php
    require_once "../vendor/autoload.php";

    use Controller\Main;

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8; ");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


    $controller = new Main();

    $status = false;

    if(!$controller->sku_exists($_POST['sku']))
    {
        $controller->insert($_POST);
        $status = true;
    }

    http_response_code(200);
    echo json_encode($status);

?>