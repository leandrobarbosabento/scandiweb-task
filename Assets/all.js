function myFunction() {
    let mylist = document.getElementById("productType");  
    let aux = mylist.options[mylist.selectedIndex].value;
    if(aux == "DVD"){
        document.getElementById("dvdAux").style.display = "block";
        document.getElementById("furnitureAux").style.display = "none";
        document.getElementById("bookAux").style.display = "none";

        document.getElementById("size").required = true;
        document.getElementById("weight").required = false;

        document.getElementById("height").required = false;
        document.getElementById("width").required = false;
        document.getElementById("length").required = false;
    }
    else {
        if(aux == "Furniture"){
            document.getElementById("dvdAux").style.display = "none";
            document.getElementById("furnitureAux").style.display = "block";
            document.getElementById("bookAux").style.display = "none";

            document.getElementById("size").required = false;
            document.getElementById("weight").required = false;

            document.getElementById("height").required = true;
            document.getElementById("width").required = true;
            document.getElementById("length").required = true;
        } else{
            if(aux == "Book"){
                document.getElementById("dvdAux").style.display = "none";
                document.getElementById("furnitureAux").style.display = "none";
                document.getElementById("bookAux").style.display = "block";

                document.getElementById("size").required = false;
                document.getElementById("weight").required = true;

                document.getElementById("height").required = false;
                document.getElementById("width").required = false;
                document.getElementById("length").required = false;
            }                
        }
    }
}

const form = document.getElementById('product_form')

form.addEventListener('submit', e => {

    e.preventDefault()

    let data = new FormData(form);
    let sku = form.sku.value;

    fetch('../API/insert.php', {
        method: 'POST',
        body: data,
    }).then(response => {
        return response.json();
    }).then(data => {

        if(data){
            window.location = "index.php"
        }
        else
        {
            document.getElementById("modal-body").innerText = "The sku " + sku + " is already in use!";
            document.getElementById("notification_button").click();
        }
    });


})
