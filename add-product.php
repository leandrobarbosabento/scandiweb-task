<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Add</title>

    <link rel="stylesheet" href="Assets/CSS/bootstrap.min.css">
    <link rel="stylesheet" href="Assets/CSS/style.css">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>

<div class="container">  
    <header class="header">
        <h1>Product Add</h1>
        <nav>
            <input value="Save" form="product_form" name="submit" type="submit" class="add-btn">
            <a href="index.php" class="delete-btn" id="delete-product-btn">Cancel</a>

            <button style="display:none" type="button" id="notification_button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Hidden Modal
            </button>
        </nav>
    </header>



    <!-- Modal for displaying message -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Alert!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modal-body">
            Sku already in use!
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>

    <form id="product_form" method="POST">
        <div class="form-group row">
            <label for="sku" class="col-sm-2 col-form-label">SKU</label>
            <div class="col-sm-5">
                <input type="text" name="sku" class="form-control" id="sku" placeholder="SKU" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-5">
                <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Price</label>
            <div class="col-sm-5">
                <input type="number" name="price" class="form-control" id="price" placeholder="Price" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="type" class="col-sm-2 col-form-label">Type Switcher</label>
            <div class="col-sm-5">
                <select name="type" class="custom-select mr-sm-2" id="productType"  onchange = "myFunction()">
                    <option value="DVD" id="DVD">DVD</option>
                    <option value="Furniture" id="Furniture">Furniture</option>
                    <option value="Book" id="Book">Book</option>
                </select>
            </div>
        </div>

        <div id="dvdAux" style="display: block">
            <div class="form-group row">
                <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
                <div class="col-sm-5">
                    <input type="number" name="size" class="form-control" id="size" placeholder="size" required>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-5 offset-sm-2">
                    <p>Please, provide size</p>
                </div>
            </div>
        </div>

        <div id="furnitureAux" style="display: none">
            <div class="form-group row">
                <label for="height" class="col-sm-2 col-form-label">Height (CM)</label>
                <div class="col-sm-5">
                    <input type="number" name="height" class="form-control" id="height" placeholder="height" >
                </div>
            </div>

            <div class="form-group row">
                <label for="width" class="col-sm-2 col-form-label">Width (CM)</label>
                <div class="col-sm-5">
                    <input type="number" name="width" class="form-control" id="width" placeholder="width" >
                </div>
            </div>

            <div class="form-group row">
                <label for="length" class="col-sm-2 col-form-label">Length (CM)</label>
                <div class="col-sm-5">
                    <input type="number" name="length" class="form-control" id="length" placeholder="length" >
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-5 offset-sm-2">
                    <p>Please, provide dimensions</p>
                </div>
            </div>
        </div>

        <div id="bookAux" style="display: none">
            <div class="form-group row">
                <label for="weight" class="col-sm-2 col-form-label">Weight (KG)</label>
                <div class="col-sm-5">
                    <input type="number" name="weight" class="form-control" id="weight" placeholder="weight" >
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-5 offset-sm-2">
                    <p>Please, provide weight</p>
                </div>
            </div>
        </div>
    </form>

</div>

<script src="Assets/all.js"></script>

</body>
</html>